package com.player.PlayerApp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.player.PlayerApp.entity.Player;
import com.player.PlayerApp.repository.PlayerRepository;

@RestController
public class PlayerController {
	
	@Autowired
	private PlayerRepository repo;
	
	@GetMapping("/players")
	public List <Player> getPlayers(@RequestParam("page_size") Integer page_size, @RequestParam("page_no") Integer page_no)
	{
		Pageable paging = PageRequest.of(page_no, page_size);
		Page <Player> res = repo.findAll(paging);
		return res.getContent();
	}
	
	@GetMapping("/playerById/{id}")
	public Optional<Player> getPlayerById(@PathVariable int id)
	{
		Optional<Player> p = repo.findById(id);
		return p;
	}

	@GetMapping("/playersByTeam")
	public List <Player> getPlayersByTeam(@RequestParam("team") String team)
	{
		List <Player> pl = repo.findByTeam(team);
		return pl;
	}
	
	@GetMapping("/playerByFirstName")
	public List <Player> getPlayerByFirstName(@RequestParam("first_name") String firstName)
	{
		List <Player> p = repo.findByFirstName(firstName);
		return p;
	}

	@GetMapping("/playerByLastName")
	public Optional <Player> getPlayerByLastName(@RequestParam("last_name") String lastName)
	{
		Optional <Player> p = repo.findByLastName(lastName);
		return p;
	}
	
	@GetMapping("/playerByNameAndTeam")
	public Optional <Player> getPlayerByNameAndTeam(@RequestParam("first_name") String firstName, @RequestParam("team") String team)
	{
		Optional <Player> p = repo.findByFirstNameAndTeam(firstName, team);
		return p;
	}
	
	@GetMapping("/playerByGoals/{goals}")
	public List <Player> getPlayersByGoals(@PathVariable int goals, @RequestParam("page_no") Integer page_no, @RequestParam("page_size") Integer page_size, @RequestParam("col") String col)
	{
		Pageable paging = PageRequest.of(page_no, page_size, Sort.by(col).descending());
		Page <Player> res = repo.findByGoalsGreaterThanEqual(goals, paging);
		return res.getContent();
	}
	
	@GetMapping("/sortBy/{col}/{order}")
	public List <Player> sort(@PathVariable String col, @PathVariable String order, @RequestParam("page_no") Integer page_no, @RequestParam("page_size") Integer page_size)
	{
		Pageable paging = null;
		if(order.equals("ascending"))
			paging = PageRequest.of(page_no, page_size, Sort.by(col).ascending());
		else if(order.equals("descending"))
			paging = PageRequest.of(page_no, page_size, Sort.by(col).descending()); 
		Page <Player> res = repo.findAll(paging);
		return res.getContent();
	}
}
