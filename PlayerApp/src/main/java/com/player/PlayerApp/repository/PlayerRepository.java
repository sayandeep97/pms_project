package com.player.PlayerApp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.player.PlayerApp.entity.Player;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Integer> {

	List <Player> findByTeam(String team);
	
	List <Player> findByFirstName(String firstName);
	
	Optional <Player> findByLastName(String lastName);
	
	Optional <Player> findByFirstNameAndTeam(String firstName, String team);
	
	Page <Player> findByGoalsGreaterThanEqual(int goals, Pageable paging);
}
